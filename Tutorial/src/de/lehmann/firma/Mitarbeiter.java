/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.lehmann.firma;

/**
 *
 * @author Sven Lehmann <SvenLehmann@amity-com.de>
 */
public class Mitarbeiter {
    private String name;
    private Auto auto;

    public Mitarbeiter(String name) {
        this.name = name;
    }

    public void autoZuteilen(Auto a) {
        if(this.auto == null) {
            this.auto = a;
        }
    }

    public void autoEntziehen() {
        this.auto = null;
    }

    public String toString() {
        return this.name;
    }
}
