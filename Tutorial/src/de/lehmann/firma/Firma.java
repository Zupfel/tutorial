/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.lehmann.firma;

/**
 *
 * @author Sven Lehmann <SvenLehmann@amity-com.de>
 */
public class Firma {
    private final Integer fuhrparkGroesse = 10;
    private Auto [] autos = new Auto[this.fuhrparkGroesse];

    public void kaufeAuto(Auto auto) {
        if(!this.contains(auto)) {
            for(Integer i = 0; i < this.fuhrparkGroesse; ++i) {
                if(this.autos[i] == null) {
                    this.autos[i] = auto;
                    return;
                }
            }
        }
    }

    public void verkaufeAuto(Auto auto) {
        for(Integer i = 0; i < this.fuhrparkGroesse; ++i) {
            if(this.autos[i] != null) {
                if(this.autos[i].hashCode() == auto.hashCode()) {
                    this.autos[i] = null;
                }
            }
        }
    }

    public void zeigeListe() {
        for(Auto a: this.autos) {
            if(a != null) {
                System.out.println(a);
            }
        }
    }

    private Boolean contains(Auto auto) {
        for(Auto a: this.autos) {
            if(a != null && a.hashCode() == auto.hashCode()) {
                return true;
            }
        }

        return false;
    }
}
