/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.lehmann.firma;

/**
 *
 * @author Sven Lehmann <SvenLehmann@amity-com.de>
 */
public class Auto {
    private String marke;
    private Mitarbeiter mitarbeiter;

    public Auto(String marke) {
        this.marke = marke;
    }

    public void zuteilen(Mitarbeiter m) {
        if(this.mitarbeiter == null) {
            this.mitarbeiter = m;
        }
    }

    @Override
    public String toString() {
        return "Das Auto " + this.marke +
               (this.mitarbeiter == null ?
                    " wird von keinem Mitarbeiter gefahren."
                :
                    " wird von dem Mitarbeiter " + this.mitarbeiter + " gefahren."
                );
    }
}
