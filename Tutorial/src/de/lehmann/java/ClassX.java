/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.lehmann.java;

/**
 *
 * @author Sven Lehmann <SvenLehmann@amity-com.de>
 */
public class ClassX {
    private ClassY ref;

    public void setLink(ClassY ref) {
        if(this.ref == null) {
            this.ref = ref;

            this.ref.setLink(this);
        }
    }

    public ClassY getLink() {
        return this.ref;
    }

    public void removeLink() {
        if(this.ref != null) {
            ClassY y = this.ref;

            this.ref = null;

            y.removeLink();
        }
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }
}
