/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.lehmann.java;

import java.util.Arrays;

/**
 *
 * @author Sven Lehmann <SvenLehmann@amity-com.de>
 */
public class ClassC {
    private Integer maxChilds = 4;
    private ClassB [] ref = new ClassB[this.maxChilds];

    private Boolean contains(ClassB ref) {
        for(ClassB o: this.ref) {
            if(o != null && o.hashCode() == ref.hashCode()) {
                return true;
            }
        }

        return false;
    }

    public void setLink(ClassB ref) {
        if(!this.contains(ref)) {
            for(Integer i = 0; i < this.maxChilds; ++i) {
                if(this.ref[i] == null) {
                    this.ref[i] = ref;
                    return;
                }
            }
        }
    }

    public ClassB getLink(Integer i) {
        return this.checkInteger(i) ? this.ref[i] : null;
    }

    public void removeLink(Integer i) {
        if(this.checkInteger(i)) {
            this.ref[i] = null;
        }
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }

    private Boolean checkInteger(Integer i) {
        return i >= 0 && i < this.maxChilds;
    }
}
