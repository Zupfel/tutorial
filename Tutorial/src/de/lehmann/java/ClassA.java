/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.lehmann.java;

/**
 *
 * @author Sven Lehmann <SvenLehmann@amity-com.de>
 */
public class ClassA {
    private ClassB ref = null;

    public void setLink(ClassB ref) {
        if(this.ref == null) {
            this.ref = ref;
        }
    }

    public ClassB getLink() {
        return this.ref;
    }

    public void removeLink() {
        this.ref = null;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + ".ref = " + this.ref.toString() + ";";
    }
}
