/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.lehmann.java;

/**
 *
 * @author Sven Lehmann <SvenLehmann@amity-com.de>
 */
public class ClassY {
    private ClassX ref = null;

    public void setLink(ClassX ref) {
        if(this.ref == null) {
            this.ref = ref;

            this.ref.setLink(this);
        }
    }

    public ClassX getLink() {
        return this.ref;
    }

    public void removeLink() {
        if(this.ref != null) {
            ClassX x = this.ref;

            this.ref = null;

            x.removeLink();
        }
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }
}
