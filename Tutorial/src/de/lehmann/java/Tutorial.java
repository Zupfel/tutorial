/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.lehmann.java;

import de.lehmann.firma.*;

/**
 *
 * @author lehmann
 */
public class Tutorial {
    public static void main(String[] args) {
/*
        ClassA a = new ClassA();
        ClassB b = new ClassB();

        a.setLink(b);

        System.out.println(a);

        if(a.getLink() != null) {
            System.out.println(b);
        }

        ClassC c = new ClassC();

        ClassB x = new ClassB();
        ClassB y = new ClassB();
        ClassB z = new ClassB();

        c.setLink(x);
        c.setLink(y);
        c.setLink(z);
        c.setLink(z);

        System.out.println(c);

        for(Integer i = 0; i < 4; ++i) {
            if(c.getLink(i) != null) {
                System.out.println(c.getLink(i).toString());
            }
        }

        ClassX t = new ClassX();
        ClassY p = new ClassY();

        t.setLink(p);

        System.out.println(t.getLink().toString());
        System.out.println(p.getLink().toString());
*/

        Firma f = new Firma();
        Auto a1 = new Auto("Audi");
        Auto a2 = new Auto("BMW");
        Auto a3 = new Auto("Mercedes");
        Mitarbeiter kai = new Mitarbeiter("Kai");
        Mitarbeiter tim = new Mitarbeiter("Tim");

        f.kaufeAuto(a1);
        f.kaufeAuto(a2);
        f.kaufeAuto(a3);

        a1.zuteilen(kai);
        a3.zuteilen(tim);

        f.verkaufeAuto(a3);

        f.zeigeListe();
    }
    
}
