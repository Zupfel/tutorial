/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.lehmann.liste;

/**
 *
 * @author Sven Lehmann <SvenLehmann@amity-com.de>
 */
public class Spielbox {
    private Spielkarte item;

    public void set(Spielkarte o) {
        this.item = o;
    }

    public Object get() {
        return this.item;
    }

    public void del() {
        this.item = null;
    }
}
