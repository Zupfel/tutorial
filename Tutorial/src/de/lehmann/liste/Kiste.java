package de.lehmann.liste;

/**
 *
 * @author Sven Lehmann <SvenLehmann@amity-com.de>
 */
public class Kiste {
    private Object item;

    public void set(Object o) {
        this.item = o;
    }

    public Object get() {
        return this.item;
    }

    public void del() {
        this.item = null;
    }
}
