/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.lehmann.liste.Fahrzeuge;

/**
 *
 * @author Sven Lehmann <SvenLehmann@amity-com.de>
 */
public class Demo {
    public static void main(String[] args) {
        Garage<Auto> garageAuto = new Garage<>();
        garageAuto.parken(new Auto());
        garageAuto.fahrzeug().fahren(656);
        System.out.println(garageAuto.fahrzeug());

        Garage<Rad> garageRad = new Garage<>();
        garageRad.parken(new Rad());
        garageRad.fahrzeug().fahren(80);
        System.out.println(garageRad.fahrzeug());

        Garage garage = new Garage();
        garage.parken(new Motorrad());
        garage.fahrzeug().fahren(4486);
        System.out.println(garage.fahrzeug());

        garage.parken(new Roller());
        System.out.println(garage.fahrzeug());
    }
}
