/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.lehmann.liste.Fahrzeuge;

/**
 *
 * @author Sven Lehmann <SvenLehmann@amity-com.de>
 */
public class Motorrad extends Fahrzeug {
    public Motorrad() {
        super(0);
    }
    
    public void fahren(Integer km) {
        this.km += km;
    }
}