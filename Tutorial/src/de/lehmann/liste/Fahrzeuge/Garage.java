/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.lehmann.liste.Fahrzeuge;

/**
 *
 * @author Sven Lehmann <SvenLehmann@amity-com.de>
 */
public class Garage<T extends Fahrzeug> {
    private T parkplatz;

    public void parken(T parkplatz) {
        this.parkplatz = parkplatz;
    }

    public void ausparken() {
        this.parkplatz = null;
    }

    public T fahrzeug() {
        return this.parkplatz;
    }
}
