/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.lehmann.liste.Fahrzeuge;

/**
 *
 * @author Sven Lehmann <SvenLehmann@amity-com.de>
 */
public abstract class Fahrzeug {
    protected Integer km;

    public Fahrzeug(Integer km) {
        this.km = km;
    }

    public abstract void fahren(Integer km);

    @Override
    public String toString() {
        return String.format(
            "%8s: %4d km gefahren.",
            this.getClass().getSimpleName(),
            this.km
        );
    }
}
