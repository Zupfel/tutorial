/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.lehmann.liste;

/**
 *
 * @author Sven Lehmann <SvenLehmann@amity-com.de>
 */
public class Auto {
    private int kilometerStand;

    public Auto(int km) {
        this.kilometerStand = km;
    }

    public void fahren(int km) {
        this.kilometerStand += km;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + ": " + this.kilometerStand + " km";
    }
}
