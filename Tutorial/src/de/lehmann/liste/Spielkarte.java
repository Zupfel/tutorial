/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.lehmann.liste;

/**
 *
 * @author Sven Lehmann <SvenLehmann@amity-com.de>
 */
public class Spielkarte<T, S> {
    private T name;
    private S number;

    public Spielkarte(T name, S number) {
        this.name = name;
        this.number = number;
    }

    @Override
    public String toString() {
        return this.name + ": " + this.number;
    }
}
