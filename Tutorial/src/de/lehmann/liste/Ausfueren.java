package de.lehmann.liste;

/**
 *
 * @author Sven Lehmann <SvenLehmann@amity-com.de>
 */
public class Ausfueren {
    public static void main(String[] args) {
        Kiste krabbelbox = new Kiste();
        krabbelbox.set(new String("Hallo Welt!"));
        System.out.println(krabbelbox.get());

        krabbelbox.set(new Auto(8752));
        System.out.println(krabbelbox.get());

        GenericBox<String> stringBox = new GenericBox<String>();
        stringBox.set(new String("Hallo Welt!"));
        System.out.println(stringBox.get());

        GenericBox<Auto> autoBox = new GenericBox<Auto>();
        autoBox.set(new Auto(21654));
        autoBox.get().fahren(12616);
        System.out.println(autoBox.get());

        Spielkarte<String, Integer> card = new Spielkarte<String, Integer>("Lusche", 0);
        System.out.println(card);

        Spielbox spielBox = new Spielbox();
        spielBox.set(card);
        System.out.println(card);
    }
}
