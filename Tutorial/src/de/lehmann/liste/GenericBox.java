package de.lehmann.liste;

/**
 *
 * @author Sven Lehmann <SvenLehmann@amity-com.de>
 */
public class GenericBox<T> {
    private T item;

    public void set(T item) {
        this.item = item;
    }

    public T get() {
        return this.item;
    }
}
