/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package singleton;

/**
 *
 * @author Sven Lehmann <SvenLehmann@amity-com.de>
 */
public class Autofabrik {
    private static final Autofabrik instance = new Autofabrik();

    private Autofabrik() {}

    public static Autofabrik getInstance() {
        return Autofabrik.instance;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return Autofabrik.instance;
    }
}
