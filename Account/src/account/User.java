/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package account;

/**
 *
 * @author Sven Lehmann <SvenLehmann@amity-com.de>
 */
public class User {
    protected String name;
    protected Integer age;
    protected Integer size;
    protected Integer weight;

    public User(String name, Integer age, Integer size, Integer weight) {
        this.name = name;
        this.age = age;
        this.size = size;
        this.weight = weight;
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public String toString() {
        return String.format(
            "%s|%d|%d|%d",
            this.name,
            this.age,
            this.size,
            this.weight
        );
    }
}
