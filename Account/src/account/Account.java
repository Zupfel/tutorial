/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package account;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Sven Lehmann <SvenLehmann@amity-com.de>
 */
public class Account {
    protected String name;
    protected String pw;
    protected Boolean active;
    protected String email;
    protected String createDate;

    public Account(String name, String pw, Boolean active, String email, Date date) {
        this.name = name;
        this.pw = pw;
        this.active = active;
        this.email = email;

        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        this.createDate = sdf.format(date);
    }

    @Override
    public int hashCode() {
        Integer result = 24;

        result = 7 * result + this.name.hashCode();
        result = 7 * result + this.pw.hashCode();
        result = 7 * result + this.email.hashCode();
        result = 7 * result + (this.active ? 1 : 0);
        result = 7 * result + this.createDate.hashCode();

        return result;
    }

    @Override
    public String toString() {
        return String.format(
            "%s|%s|%s|%b|%s",
            this.name,
            this.pw,
            this.email,
            this.active,
            this.createDate
        );
    }

    @Override
    public boolean equals(Object o) {
        return o == null
            || o == this
            || !(this.getClass() == o.getClass())
            || this.hashCode() == o.hashCode();
    }
}
