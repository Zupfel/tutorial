/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package account;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Sven Lehmann <SvenLehmann@amity-com.de>
 */
public class Demo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        List user = new ArrayList();

        Account tim = new Account("Tim", "linux", true, "tim@linux.org", new Date());
        Account bernd = new Account("Bernd", "windoof", false, "win@doof.net", new Date());
        Account markus = tim;

        user.add(tim);
        user.add(bernd);
    
        Set<Account> users = new HashSet();

        users.addAll(user);

        System.out.println(tim.equals(bernd));
        System.out.println(tim.equals(markus));

       for(Account u: users) {
           System.out.println(u);
       }
    }
}
