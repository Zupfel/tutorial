<?php
/**
 * Description of Exception
 *
 * @author Sven Lehmann <SvenLehmann@amity-com.de>
 */
class Exceptions extends Exception {
    public function __construct(
        ExceptionEnums $code,
        String $message = null,
        array $arguments = array()
    ) {
        $this->code = ExceptionEnums::get($code);

        if(is_null($message)) {
            $message = 'Uncaught exception "%s" in %s on line %d';
            $arguments = array($code->__toString(), $this->getFile(), $this->getLine());
        }

        if(!empty($vars)) {
            $this->message = call_user_func_array(array(new String($message), 'format'), $arguments);
        }
    }
}
