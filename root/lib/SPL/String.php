<?php
/**
 * Description of String
 *
 * @author Sven Lehmann <SvenLehmann@amity-com.de>
 */
class String {
    private $value;
    private $encoding = 'UTF-8';

    public function __construct($value, $encoding = null) {
        if(!is_string($value)) {
            $value = (string) $value;
        }

        $this->value = $value;

        if(is_string($encoding)) {
            $this->encoding = $encoding;
        }
        else if(function_exists('mb_internal_encoding')) {
            $this->encoding = mb_internal_encoding();
        }
    }

    public function concat() {
        $args = func_get_args();

        foreach($args as $value) {
            if(!is_string($value)) {
                $value = (string) $value;
            }

            $this->value .= $value;
        }
    }

    public function length() {
        return function_exists('mb_strlen')
            ? mb_strlen($this->value, $this->encoding)
            : strlen($this->value);
    }

    public function isEmpty() {
        return $this->length() === 0;
    }

    public function equals(&$string) {
        return $this === $string;
    }

    public function compareTo(&$string) {
        return $this->value === $string;
    }

    public function getChar(&$index) {
        if(!is_integer($index)) {
            throw new InvalidArgumentException('the index must be an integer');
        }

        if(($index < 0) || ($index >= $this->length())) {
            throw new OutOfBoundsException(
                sprintf(
                    'the index must be between 0 and %d',
                    $this->length() - 1
                )
            );
        }

        return $this->value[$index];
    }

    public function convertEncoding(&$toEncoding, &$fromEncoding = null) {
        if(function_exists('mb_convert_encoding')) {
            if(!is_string($toEncoding)) {
                throw new InvalidArgumentException('the param $toEncoding must be an string');
            }

            if(!(is_string($fromEncoding) || is_null($fromEncoding))) {
                throw new InvalidArgumentException('the param $fromEncoding must be an string');
            }

            $this->value = is_null($fromEncoding)
                ? mb_convert_encoding($this->value, $toEncoding)
                : mb_convert_encoding($this->value, $toEncoding, $fromEncoding);
        }
    }

    public function subString(&$beginIndex, $endIndex = null) {
        if(!is_integer($beginIndex)) {
            throw new InvalidArgumentException('the begin index must be an integer');
        }

        if($beginIndex < 0) {
            throw new OutOfBoundsException('the begin index must be 0 or higher');
        }

        if(is_null($endIndex)) {
            $endIndex = $this->length() - 1 - $beginIndex;
        }
        else if(!is_integer($endIndex)) {
            throw new InvalidArgumentException('the end index must be an integer');
        }

        if($endIndex - $beginIndex < 0) {
            throw new OutOfBoundsException('the sub string length must be higher as 0');
        }

        return new String(
            function_exists('mb_substr')
                ? mb_substr($this->value, $beginIndex, $endIndex, $this->encoding)
                : substr($this->value, $beginIndex, $endIndex)
        );
    }

    public function replace(&$search, &$replace = '', &$count = null) {
        if(!(is_string($search) || is_array($search))) {
            throw new InvalidArgumentException('the param $search must be an string or array');
        }

        if(!(is_string($replace) || is_array($replace))) {
            throw new InvalidArgumentException('the param $replace must be an string or array');
        }

        if(!(is_integer($count) || is_null($count))) {
            throw new InvalidArgumentException('the param $count must be an integer');
        }

        if(is_null($count)) {
            $this->value = function_exists('mb_str_replace')
                ? mb_str_replace($search, $replace, $this->value)
                : $this->value = str_replace($search, $replace, $this->value);
        }
        else {
            $this->value = function_exists('mb_str_replace')
                ? mb_str_replace($search, $replace, $this->value, $count)
                : $this->value = str_replace($search, $replace, $this->value, $count);
        }
    }

    public function regexReplace(&$pattern, &$replace, &$limit = -1) {
        if(!(is_string($pattern) || is_array($pattern))) {
            throw new InvalidArgumentException('the param $pattern must be an string or array');
        }

        if(!(is_string($replace) || is_array($replace))) {
            throw new InvalidArgumentException('the param $replace must be an string or array');
        }

        if(!is_integer($limit)) {
            throw new InvalidArgumentException('the param $limit must be an integer');
        }

        $this->value = preg_replace($pattern, $replace, $this->value, $limit);
    }

    public function match(&$pattern) {
        if(!is_string($pattern)) {
            throw new InvalidArgumentException('the param $pattern must be an string');
        }

        preg_match_all($pattern, $this->value, $matches, PREG_PATTERN_ORDER);

        return $matches[0];
    }

    public function indexOf(&$needle, &$offset = 0) {
        if(!is_string($needle)) {
            throw new InvalidArgumentException('the param $needle must be an string');
        }

        if(!is_integer($offset)) {
            throw new InvalidArgumentException('the param $offset must be an integer');
        }

        return function_exists('mb_strpos')
            ? mb_strpos($this->value, $needle, $offset, $this->encoding)
            : strpos($this->value, $needle, $offset);
    }

    public function lastIndexOf(&$needle, &$offset = 0) {
        if(!is_string($needle)) {
            throw new InvalidArgumentException('the param $needle must be an string');
        }

        if(!is_integer($offset)) {
            throw new InvalidArgumentException('the param $offset must be an integer');
        }

        return function_exists('mb_strrpos')
            ? mb_strrpos($this->value, $needle, $offset, $this->encoding)
            : strrpos($this->value, $needle, $offset);
    }

    public function toLowerCase() {
        $this->value = function_exists('mb_strtolower')
            ? mb_strtolower($this->value, $this->encoding)
            : strtolower($this->value);
    }

    public function toUpperCase() {
        $this->value = function_exists('mb_strtolower')
            ? mb_strtoupper($this->value, $this->encoding)
            : strtoupper($this->value);
    }

    public function firstLetterToUpper() {
        $this->toLowerCase();

        $firstLetter = $this->subString(0, 1);

        $newFirstLetter = new String($firstLetter);
        $newFirstLetter->toUpperCase();

        $this->replace($firstLetter, $newFirstLetter, 1);
    }

    public function split(&$delimeter = null, $limit = null) {
        if(!(is_string($delimeter) || is_null($delimeter))) {
            throw new InvalidArgumentException('the param $delimeter must be an string');
        }

        if(!(is_integer($limit) || is_null($limit))) {
            throw new InvalidArgumentException('the param $limit must be an integer');
        }

        if(is_null($delimeter)) {
            if(is_null($limit)) {
                $limit = 1;
            }

            return function_exists('mb_str_split')
                ? mb_str_split($this->value, $limit)
                : str_split($this->value, $limit);
        }

        return is_null($limit)
            ? explode($delimeter, $this->value)
            : explode($delimeter, $this->value, $limit);
    }

    public function trim($charMask = " \t\n\r\0\x0B") {
        if(!is_string($charMask)) {
            throw new InvalidArgumentException('the param $charMask must be an string');
        }

        $this->value = function_exists('mb_trim')
            ? mb_trim($this->value, $charMask)
            : trim($this->value, $charMask);
    }

    public static function format() {
        if(func_num_args() < 2) {
            throw new InvalidArgumentException('the function must be 2 or more params');
        }

        $args = func_get_args();

        $format = $args[0];

        unset($args[0]);

        return sprintf($format, $args);
    }

    public function random($size, $chars = '') {
        # do write
    }

    public static function isString($value) {
        return $value instanceof String
            || is_string($value);
    }

    public function __toString() {
        return $this->value;
    }

    public function __callStatic($name, $arguments) {
        if(!method_exists($this, $name)) {
            throw new BadMethodCallException(
                String::format('the method "%s" does not exists', $name)
            );
        }

        $nonStaticMethods = array('trim','split','firstLetterToUpper','toUpperCase','toLowerCase','lastIndexOf','indexOf','match','regexReplace','replace','subString','convertEncoding','getChar','compareTo','equals','isEmpty','length','concat');

        if(in_array($name, $nonStaticMethods)) {
            if(!is_string($arguments[0])) {
                $arguments[0] = (string) $arguments[0];
            }

            $this->value = $arguments[0];

            unset($arguments[0]);
        }

        call_user_func_array(array($this, $name), $arguments);
    }
}