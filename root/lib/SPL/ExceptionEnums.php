<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ExceptionEnums
 *
 * @author Sven Lehmann <SvenLehmann@amity-com.de>
 */
class ExceptionEnums extends Enum {
    const __default = ExceptionEnums::Error;
    const RuntimeError = 0;
    const BadFunctionCall = '';
    const BadMethodeCall = '';
    const InvalidArgument = '';
    const Length = '';
    const Range = '';
    const Logic = '';
    const OutOfBounds = '';
    const OutOfRange = '';
    const Overflow = '';
    const Underflow = '';
    const UnexpectedValue = '';
}