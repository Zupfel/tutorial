<?php
/**
 * Description of File
 *
 * @author Sven Lehmann <SvenLehmann@amity-com.de>
 */
class File {
    private $name;
    private $path = '';
    private $content = '';

    public function __construct(String $name) {
        $this->name = new String(basename($name));

        if($this->name !== $name) {
            $name->replace($this->name);

            $path = $name;
        }
    }

    public function exists() {
        return is_file($this->path . $this->name)
            && file_exists($this->path . $this->name);
    }

    public function isReadable() {
        return is_readable($this->path . $this->name);
    }

    public function isWritable() {
        return is_writable($this->path . $this->name);
    }

    public function isExecutable() {
        return is_executable($this->path . $this->name);
    }

    public function isUploaded() {
        return is_uploaded_file($this->path . $this->name);
    }

    public function read() {
        if(!$this->exists()) {
            throw new ErrorException(
                String::format('the file "%s" does not exists', $this->name)
            );
        }

        if(is_null($this->content)) {
            if($this->isUploaded()) {
                
            }
            else {
                $this->content = new String(
                    empty($this->path)
                        ? file_get_contents($this->name)
                        : file_get_contents($this->name, $this->path)
                );
            }
        }
    }

    public function add(String $line) {
        $this->content .= $line;
    }

    public function write() {
        if(!isset($this->content) || empty($this->content)) {
            throw new Exception('no content to write');
        }
    }

    public function copy($path) {
        copy($this->path . $this->name, $path);
    }

    public function delete() {
        unlink($this->path . $this->name);
    }

    public function search(String $searchString) {}

    public function type() {
        return filetype($this->path . $this->name);
    }

    public function extension() {
        return pathinfo($this->path . $this->name, PATHINFO_EXTENSION);
    }

    public function size() {
        return filesize($this->path . $this->name);
    }

    public function time() {
        return fileatime($this->path . $this->name);
    }

    public function name() {
        return $this->name;
    }

    public function rename(String $name) {
        rename($this->path . $this->name, $this->path . $name);

        $this->name = $name;
    }

    public function path() {
        return $this->path;
    }

    public function realPath() {
        return realpath($this->path . $this->name);
    }

    public function ownerID() {
        return fileowner($this->path . $this->name);
    }

    public function groupID() {
        return filegroup($this->path . $this->name);
    }

    public function __toString() {
        return $this->content;
    }

    public function __unset() {
        $this->delete();
    }
}