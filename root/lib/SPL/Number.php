<?php
/**
 * Description of Number
 *
 * @author Sven Lehmann <SvenLehmann@amity-com.de>
 */
class Number {
    private $value;

    public function compareTo(&$value) {
        return $this->value === $value;
    }

    public function add($value) {
        $this->validate($value);

        $this->value += $value;
    }

    public function subtract($value) {
        $this->validate($value);

        $this->value -= $value;
    }

    public function multiply($value) {
        $this->validate($value);

        $this->value *= $value;
    }

    public function divide($value) {
        $this->validate($value);

        $this->value /= $value;
    }

    public static function isNumber($value) {
        return $value instanceof Integer
            || $value instanceof Float;
    }

    protected function validate($value) {
        if(!Number::isNumber($value)) {
            throw new InvalidArgumentException(
                String::format(
                    'requires a number, a %s is given',
                    gettype($value)
                )
            );
        }

        if($this->value instanceof Integer) {
            if(!($value instanceof Integer)) {
                throw new InvalidArgumentException(
                   'can not count on two different data types' 
                );
            }
        }
        else if($this->value instanceof Float) {
            if(!($value instanceof Float)) {
                throw new InvalidArgumentException(
                   'can not count on two different data types' 
                );
            }
        }
    }
}
