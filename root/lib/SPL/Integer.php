<?php
/**
 * Description of Integer
 *
 * @author Sven Lehmann <SvenLehmann@amity-com.de>
 */
class Integer extends Number {
    public function __construct(&$value) {
        if(!is_integer($value)) {
            if(function_exists('settype')) {
                settype($value, 'integer');
            }
            else {
                $value = (integer) $value;
            }
        }

        $this->value = $value;
    }
    
    public function __toString() {
        return $this->value;
    }
}
