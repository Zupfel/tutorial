package fuhrparkliste;

import Fahrzeuge.Vehicle;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 *
 * @author Sven Lehmann <SvenLehmann@amity-com.de>
 */
public class Fuhrpark {
    final static String PATH = "/home/lehmann/NetBeansProjects/FuhrparkListe/src/fuhrparkliste/";
    private HashMap<Integer, Vehicle> parkplatz = new HashMap<>();

    public void kaufen(Vehicle a) {
        Iterator f = this.parkplatz.entrySet().iterator();

        while(f.hasNext()) {
            Map.Entry auto = (Map.Entry) f.next();

            if(a.equals(auto.getValue())) {
                return;
            }
        }

        this.parkplatz.put(this.parkplatz.size() + 1, a);
        this.schreiben();
    }

    public void verkaufen(Integer id) {
        Iterator a = this.parkplatz.entrySet().iterator();

        while(a.hasNext()) {
            Map.Entry auto = (Map.Entry) a.next();

            if(auto.getKey() == id) {
                a.remove();
            }
        }

        this.schreiben();
    }

    public void auflisten() {
        String line;

        System.out.println(
            String.format(
                "-----------[ %d ]-----------",
                this.parkplatz.size()
            )
        );

        try(BufferedReader f = new BufferedReader( new FileReader( Fuhrpark.PATH + "parken.txt" ) ) ) {
            while((line = f.readLine()) != null) {
                System.out.println(line);
            }
        }
        catch(Exception e) {
            System.out.println(e.getMessage());
        };
    }

    public void schreiben() {
        Iterator a = this.parkplatz.entrySet().iterator();

        try(BufferedWriter f = new BufferedWriter( new FileWriter( Fuhrpark.PATH + "parken.txt" ) ) ) {
            while(a.hasNext()) {
                Map.Entry auto = (Map.Entry) a.next();

                f.write(auto.getValue().toString());
                f.newLine();
            }
        }
        catch(Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
