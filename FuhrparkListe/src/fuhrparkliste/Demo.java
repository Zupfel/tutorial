/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fuhrparkliste;

import Fahrzeuge.Auto;
import Fahrzeuge.LKW;
import Fahrzeuge.Sprinter;

/**
 *
 * @author Sven Lehmann <SvenLehmann@amity-com.de>
 */
public class Demo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Fuhrpark f = new Fuhrpark();

        f.kaufen(new Auto());
        f.kaufen(new Auto());
        f.kaufen(new Sprinter());
        f.kaufen(new Auto());
        LKW l = new LKW();
        f.kaufen(l);
        f.kaufen(new Auto());
        f.kaufen(l);

        f.verkaufen(5);

        f.auflisten();
    }
    
}
