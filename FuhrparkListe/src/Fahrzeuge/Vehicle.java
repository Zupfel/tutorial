/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Fahrzeuge;

/**
 *
 * @author Sven Lehmann <SvenLehmann@amity-com.de>
 */
public abstract class Vehicle {
    private Integer km;

    public Vehicle(Integer km) {
        this.km = km;
    }

    public void fahren(Integer km) {
        this.km += km;
    }

    @Override
    public String toString() {
        return String.format(
            "%s ist %d km gefahren.",
            this.getClass().getSimpleName(),
            this.km
        );
    }
}
