/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Fahrzeuge;

/**
 *
 * @author Sven Lehmann <SvenLehmann@amity-com.de>
 */
public enum VehicleEnum {
    AUTO(1),
    SPRINTER(2),
    LKW(3);

    private final Integer typ;

    private VehicleEnum(Integer typ) {
        this.typ = typ;
    }

    public Integer getTyp() {
        return this.typ;
    }
}
