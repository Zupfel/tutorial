package collectiondemo;

/**
 *
 * @author Sven Lehmann <SvenLehmann@amity-com.de>
 */
public class Auto {
    private Integer km;

    public Auto(Integer km) {
        this.km = km;
    }

    public void fahren(Integer km) {
        this.km = km;
    }

    public Integer kmStand() {
        return this.km;
    }

    @Override
    public String toString() {
        return String.format(
            "%8s: %4d km",
            this.getClass().getSimpleName(),
            this.km
        );
    }
}
