package collectiondemo;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

/**
 *
 * @author Sven Lehmann <SvenLehmann@amity-com.de>
 */
public class Demo {
    public static void main(String[] args) {
        List<Auto> fuhrpark = new ArrayList<>();

        fuhrpark.add(new Auto(0));
        fuhrpark.add(new Auto(20));
        fuhrpark.add(new Auto(54));
        fuhrpark.add(new Auto(8));
        fuhrpark.add(new Auto(400));
        fuhrpark.add(2, new Auto(7));

        Iterator f = fuhrpark.iterator();

        while(f.hasNext()) {
            Random r = new Random();
            Auto a = ((Auto) (f.next()));

            a.fahren(
                r.nextInt((9999 - 1) + 1) + 1
            );

            if(a.kmStand() < 1000) {
                f.remove();
            }
        }

        Iterator a = fuhrpark.iterator();

        while(a.hasNext()) {
            System.out.println(a.next());
        }
    }
    
}
